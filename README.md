# IaC for K8s in Aliyun

## Pre-requisites

- terraform 0.12.x
- provider alicloud >=1.50
- OSS Object Storage Service, for terraform remote state files, configred in `config.tf`
- tablestore, used for terraform remote state lock, configured in `config.tf`

Known issues:

- https://github.com/terraform-providers/terraform-provider-alicloud/issues/1034

Must create cluster manually first in order to grant all the roles to your account

- https://github.com/terraform-providers/terraform-provider-alicloud/issues/2258

Container registry creation succeeded but not created. At the moment must create manually via UI.

## Deploy

```bash
export ALICLOUD_ACCESS_KEY="xxx"
export ALICLOUD_SECRET_KEY="yyy"
export ALICLOUD_REGION="cn-shenzhen"
rm -rf tfplan
terraform init
terraform plan -out tfplan
terraform apply tfplan
```

## Todo

- put network/ecr/k8s into individual
- setup directory structure for multiple ENVs
