# setting ALICLOUD_ACCESS_KEY, ALICLOUD_SECRET_KEY and ALICLOUD_REGION using ENV vars
provider "alicloud" {}

terraform {
  backend "oss" {
    # bucket must be created first before using
    bucket = "tf-state-test"
    prefix = "terraform/state"
    key    = "terraform.tfstate"
    region = "cn-shenzhen"
    # Tablestore and table must be created manually first before using
    tablestore_endpoint = "https://statelock.cn-hangzhou.ots.aliyuncs.com"
    tablestore_table    = "statelock"
  }
}
