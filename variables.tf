variable "container_registry_namespace" {
  type = string
}

variable "repo_names" {
  type = list(string)
}

variable "cluster_name" {
  type = string
}
