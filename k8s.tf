resource "alicloud_cs_managed_kubernetes" "k8s" {
  # testing, to be changed to name so that result doesn't have random string
  name                      = var.cluster_name
  availability_zone         = "cn-shenzhen-a"
  worker_vswitch_ids        = [alicloud_vswitch.az1.id, alicloud_vswitch.az2.id, alicloud_vswitch.az3.id]
  new_nat_gateway           = true
  worker_instance_types     = ["ecs.sn2ne.large"]
  worker_number             = 2
  version                   = "1.16.6-aliyun.1"
  password                  = "Yourpassword1234"
  pod_cidr                  = "172.20.0.0/16"
  service_cidr              = "172.21.0.0/20"
  install_cloud_monitor     = true
  slb_internet_enabled      = true
  worker_disk_category      = "cloud_efficiency"
  worker_data_disk_category = "cloud_ssd"
  worker_data_disk_size     = 100
}
