resource "alicloud_cr_namespace" "sei" {
  name               = var.container_registry_namespace
  auto_create        = false
  default_visibility = "PRIVATE"
}

resource "alicloud_cr_repo" "repos" {
  for_each = toset(var.repo_names)

  namespace = alicloud_cr_namespace.sei.name
  name      = each.key
  summary   = each.key
  repo_type = "PRIVATE"
  detail    = each.key
}
