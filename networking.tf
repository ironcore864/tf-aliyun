resource "alicloud_vpc" "default" {
  name       = var.cluster_name
  cidr_block = "10.1.0.0/21"
}

resource "alicloud_vswitch" "az1" {
  name              = var.cluster_name
  vpc_id            = alicloud_vpc.default.id
  cidr_block        = "10.1.1.0/24"
  availability_zone = "cn-shenzhen-a"
}

resource "alicloud_vswitch" "az2" {
  name              = var.cluster_name
  vpc_id            = alicloud_vpc.default.id
  cidr_block        = "10.1.2.0/24"
  availability_zone = "cn-shenzhen-b"
}

resource "alicloud_vswitch" "az3" {
  name              = var.cluster_name
  vpc_id            = alicloud_vpc.default.id
  cidr_block        = "10.1.3.0/24"
  availability_zone = "cn-shenzhen-c"
}
